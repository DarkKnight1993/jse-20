package ru.tsc.goloshchapov.tm.command;

import ru.tsc.goloshchapov.tm.model.Task;

public abstract class AbstractTaskCommand extends AbstractCommand {

    public void showTask(Task task) {
        System.out.println("[SELECTED TASK]");
        System.out.println(
                "Id: " + task.getId() +
                        "\nName: " + task.getName() +
                        "\nDescription: " + task.getDescription() +
                        "\nStatus: " + task.getStatus()
        );
        System.out.println("[END TASK]");
        System.out.println("[OK]");
    }

}
