package ru.tsc.goloshchapov.tm.command.task;

import ru.tsc.goloshchapov.tm.command.AbstractTaskCommand;
import ru.tsc.goloshchapov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.goloshchapov.tm.model.Task;
import ru.tsc.goloshchapov.tm.util.TerminalUtil;

public final class TaskFinishByIndexCommand extends AbstractTaskCommand {
    @Override
    public String name() {
        return "task-finish-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Finish task by index";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[FINISH TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = serviceLocator.getTaskService().finishByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("[OK]");
    }

}
