package ru.tsc.goloshchapov.tm.command.projecttask;

import ru.tsc.goloshchapov.tm.command.AbstractProjectTaskCommand;
import ru.tsc.goloshchapov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.goloshchapov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.goloshchapov.tm.model.Project;
import ru.tsc.goloshchapov.tm.model.Task;
import ru.tsc.goloshchapov.tm.util.TerminalUtil;

public final class ProjectTaskUnbindByIdCommand extends AbstractProjectTaskCommand {
    @Override
    public String name() {
        return "project-task-unbind-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Unbind task to project by id";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UNBIND TASK FROM PROJECT BY ID]");
        System.out.println("ENTER TASK ID");
        final String taskId = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("ENTER PROJECT ID");
        final String projectId = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        final Task taskUnbinded = serviceLocator.getProjectTaskService().unbindTaskById(userId, projectId, taskId);
        if (taskUnbinded == null) throw new TaskNotFoundException();
        System.out.println("[OK]");
    }

}
