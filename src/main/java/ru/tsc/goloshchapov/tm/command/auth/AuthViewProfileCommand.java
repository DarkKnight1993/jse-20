package ru.tsc.goloshchapov.tm.command.auth;

import ru.tsc.goloshchapov.tm.command.AbstractAuthCommand;
import ru.tsc.goloshchapov.tm.model.User;

public final class AuthViewProfileCommand extends AbstractAuthCommand {
    @Override
    public String name() {
        return "view-profile";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Display user profile info";
    }

    @Override
    public void execute() {
        final User user = serviceLocator.getAuthService().getUser();
        System.out.println("[VIEW PROFILE]");
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("[OK]");
    }
}
