package ru.tsc.goloshchapov.tm.command.project;

import ru.tsc.goloshchapov.tm.command.AbstractProjectCommand;
import ru.tsc.goloshchapov.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractProjectCommand {
    @Override
    public String name() {
        return "project-create";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Create a new project";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        serviceLocator.getProjectService().create(userId, name, description);
        System.out.println("[OK]");
    }

}
