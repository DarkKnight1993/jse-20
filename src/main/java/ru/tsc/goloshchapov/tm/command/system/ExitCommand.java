package ru.tsc.goloshchapov.tm.command.system;

import ru.tsc.goloshchapov.tm.command.AbstractSystemCommand;

public final class ExitCommand extends AbstractSystemCommand {
    @Override
    public String name() {
        return "exit";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Close application";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
