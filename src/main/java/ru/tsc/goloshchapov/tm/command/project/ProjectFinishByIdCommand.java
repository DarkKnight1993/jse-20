package ru.tsc.goloshchapov.tm.command.project;

import ru.tsc.goloshchapov.tm.command.AbstractProjectCommand;
import ru.tsc.goloshchapov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.goloshchapov.tm.model.Project;
import ru.tsc.goloshchapov.tm.util.TerminalUtil;

public final class ProjectFinishByIdCommand extends AbstractProjectCommand {
    @Override
    public String name() {
        return "project-finish-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Finish project by id";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[FINISH PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().finishById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("[OK]");
    }

}
