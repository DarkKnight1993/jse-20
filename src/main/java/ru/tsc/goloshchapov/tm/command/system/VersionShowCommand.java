package ru.tsc.goloshchapov.tm.command.system;

import ru.tsc.goloshchapov.tm.command.AbstractSystemCommand;

public final class VersionShowCommand extends AbstractSystemCommand {
    @Override
    public String name() {
        return "info";
    }

    @Override
    public String arg() {
        return "-v";
    }

    @Override
    public String description() {
        return "Display program version";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.0.17");
    }

}
