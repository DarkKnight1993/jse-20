package ru.tsc.goloshchapov.tm.command.task;

import ru.tsc.goloshchapov.tm.command.AbstractTaskCommand;
import ru.tsc.goloshchapov.tm.enumerated.Sort;
import ru.tsc.goloshchapov.tm.model.Task;
import ru.tsc.goloshchapov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListShowCommand extends AbstractTaskCommand {
    @Override
    public String name() {
        return "task-list";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show task list";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        List<Task> tasks;
        if (sort == null || sort.isEmpty()) tasks = serviceLocator.getTaskService().findAll(userId);
        else {
            Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            tasks = serviceLocator.getTaskService().findAll(userId, sortType.getComparator());
        }
        int index = 1;
        System.out.println("[LIST TASKS]");
        for (Task task : tasks) {
            System.out.println(index + ") " + task.toString());
            index++;
        }
        System.out.println("[END LIST]");
        System.out.println("[OK]");
    }

}
