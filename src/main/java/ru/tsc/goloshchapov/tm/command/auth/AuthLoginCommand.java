package ru.tsc.goloshchapov.tm.command.auth;

import ru.tsc.goloshchapov.tm.command.AbstractAuthCommand;
import ru.tsc.goloshchapov.tm.util.TerminalUtil;

public final class AuthLoginCommand extends AbstractAuthCommand {
    @Override
    public String name() {
        return "login";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Login user in app";
    }

    @Override
    public void execute() {
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
        System.out.println("[OK]");
    }

}
