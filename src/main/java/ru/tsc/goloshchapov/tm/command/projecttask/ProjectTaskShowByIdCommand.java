package ru.tsc.goloshchapov.tm.command.projecttask;

import ru.tsc.goloshchapov.tm.command.AbstractProjectTaskCommand;
import ru.tsc.goloshchapov.tm.model.Task;
import ru.tsc.goloshchapov.tm.util.TerminalUtil;

import java.util.List;

public final class ProjectTaskShowByIdCommand extends AbstractProjectTaskCommand {
    @Override
    public String name() {
        return "project-task-show-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show all task of project by id";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW ALL TASK OF PROJECT BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = serviceLocator.getProjectTaskService().findTaskByProjectId(userId, projectId);
        System.out.println("[TASK LIST FOR PROJECT WITH ID: " + projectId + " ]");
        int index = 1;
        for (Task task : tasks) {
            System.out.println(index + ") " + task.toString());
            index++;
        }
        System.out.println("[END LIST]");
        System.out.println("[OK]");
    }

}
