package ru.tsc.goloshchapov.tm.exception.user;

import ru.tsc.goloshchapov.tm.exception.AbstractException;

public class LoginExistsException extends AbstractException {

    public LoginExistsException() {
        super("Exception! Login is already exists!");
    }

}
