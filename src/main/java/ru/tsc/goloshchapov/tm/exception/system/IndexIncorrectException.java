package ru.tsc.goloshchapov.tm.exception.system;

import ru.tsc.goloshchapov.tm.exception.AbstractException;

public class IndexIncorrectException extends AbstractException {

    public IndexIncorrectException() {
        super("Exception! Index is incorrect!");
    }

    public IndexIncorrectException(final String value) {
        super("Exception! Current value `" + value + "` is not number!");
    }

    public IndexIncorrectException(final Throwable cause) {
        super(cause);
    }

}
