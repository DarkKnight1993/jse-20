package ru.tsc.goloshchapov.tm.repository;

import ru.tsc.goloshchapov.tm.api.repository.IAuthRepository;

public final class AuthRepository implements IAuthRepository {

    private String userId;

    @Override
    public String getUserId() {
        if (userId == null) return null;
        return userId;
    }

    @Override
    public void setUserId(final String userId) {
        this.userId = userId;
    }

    @Override
    public void clearUserId() {
        userId = null;
    }

    @Override
    public boolean isUserIdExists() {
        return userId == null;
    }
}
