package ru.tsc.goloshchapov.tm.api.service;

import ru.tsc.goloshchapov.tm.enumerated.Role;
import ru.tsc.goloshchapov.tm.model.User;

import java.util.List;

public interface IUserService {

    List<User> findAll();

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User setPassword(String userId, String password);

    User findById(String id);

    User findByLogin(String login);

    User findByEmail(String email);

    User updateUser(String userId, String firstName, String middleName, String lastName);

    User removeUser(User user);

    User removeById(String id);

    User removeByLogin(String login);

    boolean isLoginExists(String login);

    boolean isEmailExists(String email);
}
