package ru.tsc.goloshchapov.tm.api.entity;

import java.util.Date;

public interface IHasStartDate {

    Date getStartDate();

    void setStartDate(Date startDate);

}
