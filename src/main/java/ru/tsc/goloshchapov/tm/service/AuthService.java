package ru.tsc.goloshchapov.tm.service;

import ru.tsc.goloshchapov.tm.api.repository.IAuthRepository;
import ru.tsc.goloshchapov.tm.api.service.IAuthService;
import ru.tsc.goloshchapov.tm.api.service.IUserService;
import ru.tsc.goloshchapov.tm.exception.empty.EmptyLoginException;
import ru.tsc.goloshchapov.tm.exception.empty.EmptyPasswordException;
import ru.tsc.goloshchapov.tm.exception.user.AccessDeniedException;
import ru.tsc.goloshchapov.tm.model.User;
import ru.tsc.goloshchapov.tm.util.HashUtil;

public final class AuthService implements IAuthService {

    private final IUserService userService;

    private final IAuthRepository authRepository;

    public AuthService(IUserService userService, IAuthRepository authRepository) {
        this.userService = userService;
        this.authRepository = authRepository;
    }

    @Override
    public User getUser() {
        final String userId = getUserId();
        return userService.findById(userId);
    }

    @Override
    public String getUserId() {
        final String userId = authRepository.getUserId();
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return authRepository.isUserIdExists();
    }

    @Override
    public void logout() {
        authRepository.clearUserId();
    }

    @Override
    public void login(String login, String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        final String hash = HashUtil.salt(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        authRepository.setUserId(user.getId());
    }

    @Override
    public void registry(String login, String password, String email) {
        userService.create(login, password, email);
    }

    @Override
    public void updateProfile(String userId, String firstName, String middleName, String lastName) {
        userService.updateUser(userId, firstName, middleName, lastName);
    }

    @Override
    public void changePassword(String userId, String password) {
        userService.setPassword(userId, password);
    }
}
